@extends('layouts.app')

@section('content')

    @include('sidebar')


    <div class="pusher">
        <div class="ui stackable menu">
            <div class="item">
                <img src="{{url('/images/logo.png')}}">
            </div>


            <div class="right menu">

                <a class="item" >Logged in as {{Auth::user()->name}}</a>
            </div>
        </div>

        @if( Session::has('status') )
            <div class="ui green message" align="center">{{ Session::get('status')}}</div>
        @endif

        <div class="ui container grid centered" >

            <div align="center" class="feedbackHeader" style="margin-top: 20px">
                <h3>{{$feedback->title}}</h3>
                @if(Auth::user()->role != "Student")
                <h5>{{$feedback->description}}</h5>
                @endif
            </div>
            <table class="ui celled table">

                <thead>
                    <tr>
                        <th>Question</th>

                        @if(Auth::user()->role == "Student")
                            <th></th>
                        @else
                            <th>Created At</th>
                        @endif
                    </tr>
                </thead>

                @if(count($feedbackQuestions) <= 0)
                    <tr>
                        <td style="text-align: center" colspan="2">No Questions</td>
                    </tr>
                @endif
                  @foreach($feedbackQuestions as $item)
                    <tr>
                        <td>{{$item->question}}</td>

                        @if(Auth::user()->role == "Student")
                            <td>
                                <form class="ui form">

                                    <input type="hidden" value="{{$item->fqid}}" name="fqid">
                                    <div class="inline field">
                                        <div class="ui checkbox">
                                            <input  tabindex="0" class="one hidden" type="checkbox">
                                            <label>Less than 30%</label>
                                        </div>
                                        <div class="ui checkbox">
                                            <input tabindex="1" class="two hidden" type="checkbox">
                                            <label>31 to 50%</label>
                                        </div>
                                        <div class="ui checkbox">
                                            <input tabindex="2" class=" three hidden" type="checkbox">
                                            <label>51 to 70%</label>
                                        </div>
                                        <div class="ui checkbox">
                                            <input tabindex="3" class=" four hidden" type="checkbox">
                                            <label>71 to 90%</label>
                                        </div>
                                        <div class="ui checkbox">
                                            <input tabindex="4" class=" five hidden" type="checkbox">
                                            <label>91 to 100%</label>
                                        </div>

                                    </div>

                                </form>
                            </td>
                        @else
                            <td>{{$item->created_at}}</td>
                        @endif

                    </tr>
                @endforeach

            </table>

            @if(Auth::user()->role == "Feedback Analyst")

            <form method="post" class="ui form" action="{{url('/add-question')}}">

                {{csrf_field()}}
                <input type="hidden" name="fid" value="{{$feedback->fid}}">
                <label for="question">Question</label>
                <textarea  id="question" class="ui input" name="question"></textarea>
                <button  style="margin-top: 20px;" class="ui green button right" type="submit" id="addQuestion">Add</button>
            </form>

            @endif

            @if(Auth::user()->role == "Student")
                <button class="ui green button" id="submitButton">Submit</button>
            @endif

        </div>


    </div>

    <script>
        $('.ui.checkbox')
                .checkbox()
        ;

        $(document).ready(function () {

            $('#submitButton').click(function () {
                var forms = $("form");

                for(var i =0; i < forms.length; i++){
                    var fqid,one,two,three,four,five,response;
                    fqid = $(forms[i][0]).val();

                    // one answers array
                    $(forms[i][1]).each(function(){
                        one = this.checked;

                    });

                    // two answers array
                    $(forms[i][2]).each(function(){
                        two = this.checked;

                    });

                    // three answers array
                    $(forms[i][3]).each(function(){
                        three = this.checked;

                    });

                    // four answers array
                    $(forms[i][4]).each(function(){
                        four = this.checked;

                    });

                    // one answers array
                    $(forms[i][5]).each(function(){
                        five = this.checked;

                    });

                    console.log(one + " | " + two + " | " + fqid );

                    if(one == true)
                        response = 1;
                    else if (two == true )
                        response = 2;
                    else if (three == true )
                        response = 3;
                    else if (four == true )
                        response = 4;
                    else if (five == true )
                        response = 5;
                    else
                        response = 0;



                    $.ajax({
                        url:"{{url('/answer')}}",
                        method: "post",
                        data:{_token:"{{csrf_token()}}", fqid:fqid, answer: response  }
                    });


                } // end for


                setTimeout(window.location = "{{url('/refresh')}}", 1000);


            });


        })
    </script>
@endsection